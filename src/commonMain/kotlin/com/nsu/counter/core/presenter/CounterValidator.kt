package com.nsu.counter.core.presenter

import com.nsu.counter.core.i18n.StringProvider
import com.nsu.counter.core.integration.IntegrationObject
import com.nsu.counter.core.model.data.Counter
import com.nsu.counter.core.throwable.CounterException

class CounterValidator(private val counter: Counter) {
    private var stringProvider: StringProvider = IntegrationObject.stringProvider

    fun validate() {
        validateName()
        validateDescriptionString()
        validateCount()
    }

    private fun validateName() {
        if (counter.name.isBlank()) {
            throw CounterException(stringProvider.nameIsBlankWarning)
        }
    }

    private fun validateDescriptionString() { }

    private fun validateCount() {
        if (counter.count < 0) {
            throw CounterException(stringProvider.countIsNegativeWarning)
        }
    }
}