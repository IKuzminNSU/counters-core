package com.nsu.counter.core.presenter

import com.nsu.counter.core.view.View

interface Presenter<T : View> {
    val view: T

    fun onCreate()
}