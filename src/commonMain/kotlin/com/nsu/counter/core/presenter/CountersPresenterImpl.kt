package com.nsu.counter.core.presenter

import com.nsu.counter.core.model.data.Counter
import com.nsu.counter.core.model.repository.Repository
import com.nsu.counter.core.model.repository.factory.CountersRepositoryFactory
import com.nsu.counter.core.view.CountersView

class CountersPresenterImpl(override val view: CountersView) : CountersViewPresenter {
    private val counterRepositoryFactory = CountersRepositoryFactory()

    private val countersRepository: Repository<Counter> = counterRepositoryFactory.getRepository()
    private var counters: List<Counter> = countersRepository.getAll()
    override fun createCounterButtonPressed() {
        view.openCreateCounterScreen()
    }

    override fun countersSetChanged() {
        counters = countersRepository.getAll()
        view.displayCounters(counters)
    }

    override fun counterItemPressed(itemId: Int) {
        val counter = counters[itemId]
        view.openCounterScreen(counter)
    }

    override fun onCreate() {
        view.displayCounters(counters)
    }
}