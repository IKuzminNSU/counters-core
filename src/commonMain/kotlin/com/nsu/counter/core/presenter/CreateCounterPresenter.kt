package com.nsu.counter.core.presenter

import com.nsu.counter.core.view.CreateCounterView

interface CreateCounterPresenter : Presenter<CreateCounterView> {
    fun tryToSaveCounter()

    fun leaveAttemptPerformed()

    fun leaveConfirmed()
}