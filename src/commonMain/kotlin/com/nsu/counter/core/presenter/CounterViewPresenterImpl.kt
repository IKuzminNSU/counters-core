package com.nsu.counter.core.presenter

import com.nsu.counter.core.model.data.Counter
import com.nsu.counter.core.model.repository.factory.CountersRepositoryFactory
import com.nsu.counter.core.throwable.CounterException
import com.nsu.counter.core.view.CounterView

class CounterViewPresenterImpl(
    override val view: CounterView,
    private val counter: Counter
) : CounterViewPresenter {
    private val repositoryFactory = CountersRepositoryFactory()
    private val repository = repositoryFactory.getRepository()
    private val counterValidator = CounterValidator(counter)

    override fun incrementButtonPressed() {
        counter.count++
        view.setCount(counter.count)
    }

    override fun decrementButtonPressed() {
        if (counter.count > 0) {
            counter.count--
        }
        view.setCount(counter.count)
    }

    override fun leaveAttemptPerformed() = try {
        fetchCounterDataFromView()
        validateCounter()
        saveCounterData()
        view.showCountersView()
    } catch (ex: CounterException) {
        view.showInvalidDataWarning(ex.message)
    }

    private fun fetchCounterDataFromView() {
        counter.name = view.getName()
        counter.descriptionString = view.getDescriptionString()
        counter.count = view.getCount()
    }

    private fun validateCounter() = counterValidator.validate()

    private fun saveCounterData() = repository.save(counter)

    override fun leaveWithoutSaving() {
        view.showCountersView()
    }

    override fun onCreate() {
        displayCounterData()
    }

    private fun displayCounterData() {
        view.setName(counter.name)
        view.setDescriptionString(counter.descriptionString)
        view.setCount(counter.count)
    }
}