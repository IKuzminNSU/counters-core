package com.nsu.counter.core.presenter.factory

import com.nsu.counter.core.presenter.CountersPresenterImpl
import com.nsu.counter.core.presenter.CountersViewPresenter
import com.nsu.counter.core.view.CountersView

class CountersViewPresenterFactory(override val view: CountersView) : PresenterFactory<CountersView> {
    override fun getPresenter(): CountersViewPresenter {
        return CountersPresenterImpl(view)
    }
}