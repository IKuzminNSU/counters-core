package com.nsu.counter.core.presenter.factory

import com.nsu.counter.core.presenter.Presenter
import com.nsu.counter.core.view.View

interface PresenterFactory<V : View> {
    val view: V

    fun getPresenter(): Presenter<V>
}