package com.nsu.counter.core.presenter

import com.nsu.counter.core.view.CountersView

interface CountersViewPresenter : Presenter<CountersView> {
    fun createCounterButtonPressed()

    fun counterItemPressed(itemId: Int)

    fun countersSetChanged()
}