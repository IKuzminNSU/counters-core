package com.nsu.counter.core.presenter

import com.nsu.counter.core.model.data.Counter
import com.nsu.counter.core.model.repository.factory.CountersRepositoryFactory
import com.nsu.counter.core.throwable.CounterException
import com.nsu.counter.core.view.CreateCounterView

class CreateCounterPresenterImpl(override val view: CreateCounterView) : CreateCounterPresenter {
    private val repositoryFactory = CountersRepositoryFactory()
    private val repository = repositoryFactory.getRepository()

    override fun tryToSaveCounter() = try {
        val data = getCounterFromView()
        validateCounterData(data)
        saveCounter(data)
        view.showCountersView()
    } catch (ex: CounterException) {
        view.showInvalidDataWarning(ex.message)
    }

    private fun getCounterFromView() = Counter(
        view.getName(),
        view.getDescriptionString(),
        view.getCount()
    )

    private fun validateCounterData(counter: Counter) = CounterValidator(counter).validate()

    private fun saveCounter(counter: Counter) {
        repository.save(counter)
    }

    override fun leaveAttemptPerformed() {
        view.showLeaveWarningDialog()
    }

    override fun leaveConfirmed() {
        view.showCountersView()
    }

    override fun onCreate() { }
}