package com.nsu.counter.core.presenter.factory

import com.nsu.counter.core.model.data.Counter
import com.nsu.counter.core.presenter.CounterViewPresenter
import com.nsu.counter.core.presenter.CounterViewPresenterImpl
import com.nsu.counter.core.view.CounterView

class CounterViewPresenterFactory(override val view: CounterView, val counter: Counter) : PresenterFactory<CounterView> {
    override fun getPresenter(): CounterViewPresenter {
        return CounterViewPresenterImpl(view, counter)
    }
}