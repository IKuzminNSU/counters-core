package com.nsu.counter.core.presenter

import com.nsu.counter.core.view.CounterView

interface CounterViewPresenter : Presenter<CounterView> {
    fun incrementButtonPressed()
    fun decrementButtonPressed()
    fun leaveAttemptPerformed()
    fun leaveWithoutSaving()
}