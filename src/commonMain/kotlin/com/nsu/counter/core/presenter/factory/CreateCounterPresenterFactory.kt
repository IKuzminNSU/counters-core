package com.nsu.counter.core.presenter.factory

import com.nsu.counter.core.presenter.CreateCounterPresenter
import com.nsu.counter.core.presenter.CreateCounterPresenterImpl
import com.nsu.counter.core.view.CreateCounterView

class CreateCounterPresenterFactory(override val view: CreateCounterView) : PresenterFactory<CreateCounterView> {
    override fun getPresenter(): CreateCounterPresenter {
        return CreateCounterPresenterImpl(view)
    }
}