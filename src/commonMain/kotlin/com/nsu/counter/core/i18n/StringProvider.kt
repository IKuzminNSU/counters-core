package com.nsu.counter.core.i18n

interface StringProvider {
    val createCounterUnsavedDataWarning: String
    val nameIsBlankWarning: String
    val countIsNegativeWarning: String
}