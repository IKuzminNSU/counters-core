package com.nsu.counter.core.throwable

class CounterException(override val message: String = "") : Exception(message)