package com.nsu.counter.core.view

import com.nsu.counter.core.model.data.Counter

interface CountersView : View {
    fun openCreateCounterScreen()

    fun openCounterScreen(counter: Counter)

    fun displayCounters(counters: List<Counter>)
}