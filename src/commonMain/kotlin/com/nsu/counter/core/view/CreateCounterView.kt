package com.nsu.counter.core.view

interface CreateCounterView : View {
    fun getName(): String

    fun getDescriptionString(): String

    fun getCount(): Int

    fun showCountersView()

    fun showLeaveWarningDialog()

    fun showInvalidDataWarning(message: String)
}