package com.nsu.counter.core.view

interface CounterView : View {
    fun setName(name: String)
    fun setDescriptionString(descriptionString: String)
    fun setCount(count: Int)
    fun getName(): String
    fun getDescriptionString(): String
    fun getCount(): Int
    fun showInvalidDataWarning(message: String)
    fun showCountersView()
}