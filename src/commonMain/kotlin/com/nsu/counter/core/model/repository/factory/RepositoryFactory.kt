package com.nsu.counter.core.model.repository.factory

import com.nsu.counter.core.model.repository.Repository

interface RepositoryFactory<T> {
    fun getRepository(): Repository<T>
}