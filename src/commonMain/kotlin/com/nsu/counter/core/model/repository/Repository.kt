package com.nsu.counter.core.model.repository

interface Repository<T> {
    fun save(member: T)

    fun getAll(): List<T>

    fun findById(id: Int): T
}