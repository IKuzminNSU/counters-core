package com.nsu.counter.core.model.dbmodel

import com.nsu.counter.core.model.data.Counter

class DBCounter(
    id: Int,
    val name: String,
    val descriptionString: String,
    val count: Int
) : DBModel(id) {
    fun toSimpleModel(): Counter {
        return Counter(name, descriptionString, count, id)
    }
}