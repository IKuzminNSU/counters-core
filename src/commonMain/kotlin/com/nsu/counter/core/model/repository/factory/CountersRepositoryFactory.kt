package com.nsu.counter.core.model.repository.factory

import com.nsu.counter.core.model.data.Counter
import com.nsu.counter.core.model.repository.CountersDBRepository
import com.nsu.counter.core.model.repository.Repository

class CountersRepositoryFactory : RepositoryFactory<Counter> {
    override fun getRepository(): Repository<Counter> {
        return CountersDBRepository()
    }
}