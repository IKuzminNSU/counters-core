package com.nsu.counter.core.model.repository

import com.nsu.counter.core.integration.IntegrationObject
import com.nsu.counter.core.model.DBDriver
import com.nsu.counter.core.model.data.Counter
import com.nsu.counter.core.model.dbmodel.DBCounter
import com.nsu.counter.core.throwable.CounterException

class CountersDBRepository : Repository<Counter> {
    private var dbDriver: DBDriver<DBCounter> = IntegrationObject.counterDBDriver

    override fun save(member: Counter) {
        val dbModel = member.toDBModel()
        if (dbModel.id == 0) {
            dbDriver.insert(dbModel)
        } else {
            dbDriver.patch(dbModel)
        }
    }

    override fun getAll(): List<Counter> {
        return dbDriver.selectAll().map { it.toSimpleModel() }
    }

    override fun findById(id: Int): Counter {
        val dbModel = dbDriver.select { it.id == id }.firstOrNull()
            ?: throw CounterException("Could not find the counter with given id")
        return dbModel.toSimpleModel()
    }
}