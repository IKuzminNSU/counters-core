package com.nsu.counter.core.model.data

import com.nsu.counter.core.model.dbmodel.DBCounter

class Counter(
    var name: String,
    var descriptionString: String,
    var count: Int,
    private val dbModelId: Int = 0
) {
    fun toDBModel(): DBCounter {
        return DBCounter(dbModelId, name, descriptionString, count)
    }
}