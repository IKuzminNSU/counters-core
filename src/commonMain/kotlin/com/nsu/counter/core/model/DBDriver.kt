package com.nsu.counter.core.model
import com.nsu.counter.core.model.dbmodel.DBModel

interface DBDriver<T : DBModel> {
    fun insert(model: T)

    fun select(predicate: (T) -> Boolean): List<T>

    fun selectAll(): List<T>

    fun patch(model: T)

    fun delete(model: T)
}