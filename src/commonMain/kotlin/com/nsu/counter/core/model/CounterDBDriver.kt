package com.nsu.counter.core.model

import com.nsu.counter.core.model.dbmodel.DBCounter

interface CounterDBDriver : DBDriver<DBCounter> {
    override fun delete(model: DBCounter)
    override fun insert(model: DBCounter)
    override fun patch(model: DBCounter)
    override fun select(predicate: (DBCounter) -> Boolean): List<DBCounter>
    override fun selectAll(): List<DBCounter>
}