package com.nsu.counter.core.integration

import com.nsu.counter.core.i18n.StringProvider
import com.nsu.counter.core.model.CounterDBDriver
import com.nsu.counter.core.model.DBDriver
import com.nsu.counter.core.model.dbmodel.DBCounter

object IntegrationObject {
    lateinit var stringProvider: StringProvider
    lateinit var counterDBDriver: CounterDBDriver
}